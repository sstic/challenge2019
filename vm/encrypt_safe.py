import sys
import os
from Crypto.Cipher import AES

clear = open(sys.argv[1],"rb").read()
clear = clear + "\x00" * (16 - (len(clear)%16))
encrypted = open(sys.argv[2],"wb")
iv = os.urandom(0x10)
aes = AES.new(sys.argv[3].decode("hex"),AES.MODE_CBC,iv)
encrypted.write(iv)
encrypted.write(aes.encrypt(clear))

encrypted.close()


