#!/usr/bin/env python
import sys
from polytools import *
import struct

OPCODE_MOV=0x0
OPCODE_DEC=0x1
OPCODE_ADD=0x2
OPCODE_SUB=0x3
OPCODE_SHL=0x4
OPCODE_SHR=0x5
OPCODE_XOR=0x6
OPCODE_AND=0x7
OPCODE_JMP=0x8
OPCODE_JNZ=0x9
OPCODE_EXT=0xA
OPCODE_ROT=0xB
OPCODE_LD=0xC
OPCODE_RTC_START=0xD
OPCODE_RTC_STOP=0xE

OPTYPE_REGISTER_2_REGISTER=0
OPTYPE_MEMORY_2_REGISTER=1
OPTYPE_REGISTER_2_MEMORY=2
OPTYPE_IMMEDIATE_2_REGISTER=3

def assemble(line, labels):
    mnemonic = line.split(" ")[0].lower()

    opcode = 0
    optype = 0
    dest_r = 0
    src_r  = 0
    imm    = 0
    imm_len = 10
    if mnemonic == 'mov':
        opcode = OPCODE_MOV
        args = line[4:].split(', ')
        if len(args) != 2:
            error("Bad instruction : %s" % line)
            return
        if args[0].startswith('r') and args[1].startswith('r'):
            optype=OPTYPE_REGISTER_2_REGISTER
        elif args[0].startswith('r') and args[1].startswith('['):
            optype=OPTYPE_MEMORY_2_REGISTER
            args[1] = args[1][1:].split("]")[0]
        elif args[0].startswith('[') and args[1].startswith('r'):
            optype=OPTYPE_REGISTER_2_MEMORY
            args[0] = args[0][1:].split("]")[0]
        elif args[0].startswith('r') and args[1].startswith('0x'):
            optype=OPTYPE_IMMEDIATE_2_REGISTER
        else:
            error("Bad instruction : %s" % line)
            return
        
        dest_r = int(args[0][1:])
        if optype != OPTYPE_IMMEDIATE_2_REGISTER:
            src_r = int(args[1][1:])
        else:
            imm = int(args[1], 16)
            if imm > 16383:
                error("Bad immediate value")
                return
            imm_len = 14
    elif mnemonic == 'xor':
        opcode = OPCODE_XOR
        args = line[4:].split(', ')
        if len(args) != 2:
            error("Bad instruction : %s" % line)
            return
        if args[0].startswith('r') and args[1].startswith('r'):
            optype=OPTYPE_REGISTER_2_REGISTER
        else:
            error("Bad instruction : %s" % line)
            return
        
        dest_r = int(args[0][1:])
        if optype != OPTYPE_IMMEDIATE_2_REGISTER:
            src_r = int(args[1][1:])
        else:
            imm = int(args[1], 16)
            if imm > 16383:
                error("Bad immediate value")
                return
            imm_len = 14
    elif mnemonic == 'dec':
        opcode = OPCODE_DEC
        args = line[4:].split(', ')
        if args[0].startswith('r'):
            optype=OPTYPE_REGISTER_2_REGISTER
        else:
            error("Bad instruction : %s" % line)
            return
        
        dest_r = int(args[0][1:])
    elif mnemonic == 'add':
        opcode = OPCODE_ADD
        args = line[4:].split(', ')
        if len(args) != 2:
            error("Bad instruction : %s" % line)
            return
        if args[0].startswith('r') and args[1].startswith('r'):
            optype=OPTYPE_REGISTER_2_REGISTER
        elif args[0].startswith('r') and args[1].startswith('0x'):
            optype=OPTYPE_IMMEDIATE_2_REGISTER
        else:
            error("Bad instruction : %s" % line)
            return
        
        dest_r = int(args[0][1:])
        if optype != OPTYPE_IMMEDIATE_2_REGISTER:
            src_r = int(args[1][1:])
        else:
            imm = int(args[1], 16)
            if imm > 16383:
                error("Bad immediate value")
                return
            imm_len = 14
    elif mnemonic == 'sub':
        opcode = OPCODE_SUB
        args = line[4:].split(', ')
        if len(args) != 2:
            error("Bad instruction : %s" % line)
            return
        if args[0].startswith('r') and args[1].startswith('r'):
            optype=OPTYPE_REGISTER_2_REGISTER
        elif args[0].startswith('r') and args[1].startswith('0x'):
            optype=OPTYPE_IMMEDIATE_2_REGISTER
        else:
            error("Bad instruction : %s" % line)
            return
        
        dest_r = int(args[0][1:])
        if optype != OPTYPE_IMMEDIATE_2_REGISTER:
            src_r = int(args[1][1:])
        else:
            imm = int(args[1], 16)
            if imm > 16383:
                error("Bad immediate value")
                return
            imm_len = 14
    elif mnemonic == 'shl':
        opcode = OPCODE_SHL
        args = line[4:].split(', ')
        if len(args) != 2:
            error("Bad instruction : %s" % line)
            return
        if args[0].startswith('r') and args[1].startswith('0x'):
            optype=OPTYPE_IMMEDIATE_2_REGISTER
        else:
            error("Bad instruction : %s" % line)
            return
        
        dest_r = int(args[0][1:])
        if optype != OPTYPE_IMMEDIATE_2_REGISTER:
            src_r = int(args[1][1:])
        else:
            imm = int(args[1], 16)
            if imm > 16383:
                error("Bad immediate value")
                return
            imm_len = 14
    elif mnemonic == 'shr':
        opcode = OPCODE_SHR
        args = line[4:].split(', ')
        if len(args) != 2:
            error("Bad instruction : %s" % line)
            return
        if args[0].startswith('r') and args[1].startswith('0x'):
            optype=OPTYPE_IMMEDIATE_2_REGISTER
        else:
            error("Bad instruction : %s" % line)
            return
        
        dest_r = int(args[0][1:])
        if optype != OPTYPE_IMMEDIATE_2_REGISTER:
            src_r = int(args[1][1:])
        else:
            imm = int(args[1], 16)
            if imm > 16383:
                error("Bad immediate value")
                return
            imm_len = 14
    elif mnemonic == 'and':
        opcode = OPCODE_AND
        args = line[4:].split(', ')
        if len(args) != 2:
            error("Bad instruction : %s" % line)
            return
        if args[0].startswith('r') and args[1].startswith('0x'):
            optype=OPTYPE_IMMEDIATE_2_REGISTER
        else:
            error("Bad instruction : %s" % line)
            return
        
        dest_r = int(args[0][1:])
        if optype != OPTYPE_IMMEDIATE_2_REGISTER:
            src_r = int(args[1][1:])
        else:
            imm = int(args[1], 16)
            if imm > 16383:
                error("Bad immediate value")
                return
            imm_len = 14
    elif mnemonic == 'jmp':
        opcode = OPCODE_JMP
        label = line[4:]
        optype=OPTYPE_IMMEDIATE_2_REGISTER
        if label not in labels.keys():
            error("Can't find label : %s" % (label))
            return
        dest_r = 0
        imm = labels[label]
        if imm > 16383:
            error("Bad immediate value")
            return
        imm_len = 14
    elif mnemonic == 'jnz':
        opcode = OPCODE_JNZ
        args = line[4:].split(', ')
        label = args[1]
        if not args[0].startswith('r'):
            error("Bad instruction : %s" % line)
            return
        optype=OPTYPE_IMMEDIATE_2_REGISTER
        if label not in labels.keys():
            error("Can't find label : %s" % (label))
            return
        dest_r = int(args[0][1:])
        imm = labels[label]
        if imm > 16383:
            error("Bad immediate value")
            return
        imm_len = 14
    elif mnemonic == 'vm_exit':
        opcode = OPCODE_EXT
        optype=OPTYPE_MEMORY_2_REGISTER
        dest_r = 5
    elif mnemonic == 'ld':
        opcode = OPCODE_LD
        args = line[3:].split(', ')
        if args[0].startswith('r'):
            optype=OPTYPE_IMMEDIATE_2_REGISTER
        else:
            error("Bad instruction : %s" % line)
            return
        dest_r = int(args[0][1:])
    elif mnemonic == 'rtc_start':
        opcode = OPCODE_RTC_START
        optype=OPTYPE_IMMEDIATE_2_REGISTER
        dest_r = 4
    elif mnemonic == 'rtc_stop':
        opcode = OPCODE_RTC_STOP
        args = line[9:].split(', ')
        label = args[1]
        if args[0].startswith('r'):
            optype=OPTYPE_IMMEDIATE_2_REGISTER
        else:
            error("Bad instruction : %s" % line)
            return
        if label not in labels.keys():
            error("Can't find label : %s" % (label))
            return
        imm = labels[label]
        if imm > 16383:
            error("Bad immediate value")
            return
        imm_len = 14
        dest_r = int(args[0][1:])
    elif mnemonic == 'rot':
        opcode = OPCODE_ROT
        args = line[4:].split(', ')
        if args[0].startswith('r'):
            optype=OPTYPE_REGISTER_2_REGISTER
        else:
            error("Bad instruction : %s" % line)
            return
        dest_r = int(args[0][1:])
    else:
        error("Unsupported inst : %s" % line)
        return


    # .------------------------.
    # |OOOOSSRRRRrrrrIIIIIIIIII|
    # '------------------------'
        
    instru = opcode << 20
    instru |= optype << 18
    instru |= dest_r << 14
    if imm_len == 10:
        instru |= src_r << 10
    else:
        instru |= imm
        
    pinstru = struct.pack("<I", instru)
    print "%s : %s" % (pinstru[:3].encode("hex"), line)



    return pinstru[:3]

def gen_code(filename):
    lines = open(filename, "r").readlines()
    labels={}
    code=""

    # labels
    pos=0
    for line in lines:
        line=line.strip("\n").replace("\t", "").replace("    ", "")
        if len(line) == 0:
            continue
        if line[0] == ';':
            continue
        if line.endswith(":"):
            label_name = line.split(':')[0]
            labels[label_name]=pos
            continue
        pos+=3

    print labels

    # assemble
    for line in lines:
        line=line.strip("\n").replace("\t", "").replace("    ", "")
        if len(line) == 0:
            continue
        if line[0] == ';':
            continue
        if line.endswith(":"):
            continue
        assembled_line = assemble(line, labels)
        if assembled_line == None:
            error("Can't assemble :(")
            return
        code += assembled_line
    return code
        
