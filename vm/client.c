#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <linux/ioctl.h>
#include <sys/ioctl.h>
#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include "vm_data.h"


#define SSTIC_IOC_MAGIC   'S'
#define SSTIC_LOAD_MEM    _IOWR(SSTIC_IOC_MAGIC, 0, struct sstic_msg)
#define SSTIC_LOAD_INPUT  _IOWR(SSTIC_IOC_MAGIC, 1, struct sstic_msg)
#define SSTIC_FETCH       _IOWR(SSTIC_IOC_MAGIC, 2, struct sstic_msg)
#define SSTIC_STEP        _IOWR(SSTIC_IOC_MAGIC, 3, struct sstic_msg)
#define SSTIC_TEST        _IOWR(SSTIC_IOC_MAGIC, 4, struct sstic_msg)

// /root/client acadaa8b5b55306fb3c6dfc3b2d1c80770084644225febd71a9189aa26ec740e


struct sstic_load_mem {
	uintptr_t zone_ptr;
	uint64_t zone_len;
};

struct sstic_fetch {
	uint32_t result;
};

struct sstic_step {
	uint32_t opcode;
};

struct sstic_msg {
	union {
		struct sstic_load_mem load_mem;
		struct sstic_load_mem load_input;
		struct sstic_fetch fetch;
		struct sstic_step step;
	};
};




int decode_hex(char * src, char * dst, int len) {
	int i;
	unsigned char out,c1,c2;

	for(i=0; i<len; i++) {
		out=0;
		c1 = src[i*2];
		c2 = src[i*2+1];
		// A-Z
		if ( c1 > 64 && c1 < 71 ) {
			out+=(c1-65+0xA)<<4;
		// a-z
		}else if( c1 > 96 && c1 < 103) {
			out+=(c1-97+0xA)<<4;
		// 0-9
		}else if( c1 > 47 && c1 < 58) {
			out+=(c1-48)<<4;
		}else{
			return 0;
		}
		// A-Z
		if ( c2 > 64 && c2 < 71 ) {
			out+=(c2-65+0xA);
		// a-z
		}else if( c2 > 96 && c2 < 103) {
			out+=(c2-97+0xA);
		// 0-9
		}else if( c2 > 47 && c2 < 58) {
			out+=(c2-48);
		}else{
			return 0;
		}
		dst[i] = out;
	}
	return 1;
}


int main(int argc, char const *argv[])
{
    int ret;
    struct sstic_msg msg = {0};
    uint8_t input_ok[0x20] = {0};

    if(argc != 2 || (argc == 2 && strlen(argv[1]) != 64)) {
        printf("usage: %s [32-bytes-key-hex-encoded]\n", argv[0]);
        return 1;
    }
    if(!decode_hex(argv[1], &input_ok, 32)) {
        printf("can't decode hex\n");
        return 1;
    }


    int fd = open("/dev/sstic",O_RDONLY);
    //printf("Return open : %08x\n",fd);
    

    //printf("Load mem\n");
    msg.load_mem.zone_ptr = (uintptr_t)vm_data;
    msg.load_mem.zone_len = sizeof(vm_data);
    ioctl(fd, SSTIC_LOAD_MEM, &msg);
    
    //printf("------------- TEST WIN  ------------\n");
    //printf("Load input\n");
    msg.load_mem.zone_ptr = (uintptr_t)(&input_ok);
    msg.load_mem.zone_len = 0x20;
    ioctl(fd, SSTIC_LOAD_INPUT, &msg);

    while(1) {
        // usleep(2000); // to test anti-debug
        ioctl(fd, SSTIC_FETCH, 0);
        ret = ioctl(fd,SSTIC_STEP,0);
        if((ret&0xffff)==1){
            //printf("ret = %08x\n", ret);
            if ((ret&0xffff0000)==0){
                printf("Win\n");
                break;
            }else{
                printf("Loose\n");
                break;
            }
        }
        if((ret&0xffff)==0xffff){
            printf("Failure\n");
            break;
        }
    }

    return 0;
}
