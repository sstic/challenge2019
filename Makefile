mkfile_path := $(abspath $(lastword $(MAKEFILE_LIST)))
mkfile_dir := $(dir $(mkfile_path))

TOOLCHAIN_AARCH64 := $(mkfile_dir)/aarch64-toolchain/gcc-arm-8.2-2018.08-x86_64-aarch64-elf/bin/aarch64-elf-

all: atf_clean vmclient kernel rootfs edk atf

light: atf_clean atf

rootfs:
	aarch64-toolchain/gcc-arm-8.2-2018.08-x86_64-aarch64-elf/bin/aarch64-elf-strip --strip-debug buildroot/overlay/lib/sstic.ko
	make -C buildroot/buildroot

vmclient:
	python2 vm/do_all.py vm/decrypt.S vm/vm.bin
	make -C ./vm
	cp vm/vm.bin buildroot/overlay/root/vm.bin
	aarch64-toolchain/gcc-arm-8.2-2018.08-x86_64-aarch64-elf/bin/aarch64-elf-strip -s vm/client
	rm -rf buildroot/overlay/root
	rm -rf buildroot/buildroot/output/target/root/
	mkdir -p buildroot/overlay/root
	mkdir -p buildroot/overlay/root/safe_01
	mkdir -p buildroot/overlay/root/safe_02
	mkdir -p buildroot/overlay/root/safe_03
	mkdir -p buildroot/overlay/root/tools
	cp sstic-step2/epreuve.py buildroot/overlay/root/get_safe1_key.py
	cp sstic-step2/Main.png buildroot/overlay/root/schematics.png
	cp vm/add_key.py buildroot/overlay/root/tools
	cp vm/keystore.py buildroot/overlay/root/tools
	cp vm/decrypt_containers.py buildroot/overlay/root/tools
	chmod +x  buildroot/overlay/root/tools/*.py
	chmod +x  buildroot/overlay/root/get_safe1_key.py
	python2 vm/encrypt_safe.py sstic-step3/step3 buildroot/overlay/root/safe_01/.encrypted 5fb3a83d1fd97137076019ad6e96c6a366fb6b32618d162e00cdee9bad427a8a
	python2 vm/encrypt_safe.py vm/client buildroot/overlay/root/safe_02/.encrypted 53535449437b44773472665f564d5f31735f636f306c5f69736e5f745f49747d
	python2 vm/encrypt_safe.py vm/final.txt buildroot/overlay/root/safe_03/.encrypted acadaa8b5b55306fb3c6dfc3b2d1c80770084644225febd71a9189aa26ec740e




kernel:
	cd linux; bash ./build.sh; cd ..

edk:
	cd edk2; bash ./build.sh; cd ..

atf:
	python2 arm-trusted-firmware/tools/shellcodes.py arm-trusted-firmware/bl32/tsp/shellcodes.h
	make -C ./arm-trusted-firmware CROSS_COMPILE=$(TOOLCHAIN_AARCH64) PLAT=qemu LOG_LEVEL=20 SPD=tspd
	python2 assemble_bl33.py $(mkfile_dir)/edk2/Build/ArmVirtQemuKernel-AARCH64/RELEASE_GCC49/FV/QEMU_EFI.fd $(mkfile_dir)/linux/arch/arm64/boot/Image $(mkfile_dir)/buildroot/buildroot/output/images/rootfs.cpio.gz $(mkfile_dir)/binaries/bl33.bin
	make -C ./arm-trusted-firmware CROSS_COMPILE=$(TOOLCHAIN_AARCH64) PLAT=qemu LOG_LEVEL=20 SPD=tspd BL33=$(mkfile_dir)/binaries/bl33.bin fip
	cp arm-trusted-firmware/build/qemu/release/fip.bin ./os/flash.bin
	cp arm-trusted-firmware/build/qemu/release/bl1.bin ./os/rom.bin


atf_clean:
	make -C ./arm-trusted-firmware CROSS_COMPILE=$(TOOLCHAIN_AARCH64) PLAT=qemu SPD=tspd clean


clean: atf_clean



