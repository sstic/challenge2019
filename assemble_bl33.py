#!/bin/python2
import sys

EFI_MAX_LEN   = 0x220000
LINUX_MAX_LEN = 0xA00000
INITRD_MAX_LEN= 0x1600000

efi_f = sys.argv[1]
linux_f = sys.argv[2]
initrd_f = sys.argv[3]
out_f = sys.argv[4]

efi_data = open(efi_f, "r").read()
if len(efi_data) > EFI_MAX_LEN:
	print "Bad len"
	sys.exit(1)
efi_data += "\x00" * (EFI_MAX_LEN - len(efi_data))


linux_data = open(linux_f, "r").read()
if len(linux_data) > LINUX_MAX_LEN:
	print "Bad len"
	sys.exit(1)
linux_data += "\x00" * (LINUX_MAX_LEN - len(linux_data))

initrd_data = open(initrd_f, "r").read()
if len(initrd_data) > INITRD_MAX_LEN:
	print "Bad len"
	sys.exit(1)
initrd_data += "\x00" * (INITRD_MAX_LEN - len(initrd_data))


f=open(out_f,"w")
f.write(efi_data)
f.write(linux_data)
f.write(initrd_data)
f.close()


print "EFI ADDR : 0x%08x" % (0x60000000)
print "LINUX ADDR : 0x%08x" % (0x60000000 + EFI_MAX_LEN)
print "INITRD ADDR : 0x%08x" % (0x60000000 + EFI_MAX_LEN + LINUX_MAX_LEN)

