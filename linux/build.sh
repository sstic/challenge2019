#!/bin/sh

export CROSS_COMPILE=$(pwd)/../aarch64-toolchain/gcc-arm-8.2-2018.08-x86_64-aarch64-linux-gnu/bin/aarch64-linux-gnu-

make ARCH=arm64 CROSS_COMPILE=$CROSS_COMPILE -j 15

cp drivers/tee/sstic/sstic.ko ../buildroot/overlay/lib/sstic.ko
