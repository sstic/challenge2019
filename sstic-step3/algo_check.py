import struct
#r15
table_enc8 = [1218305502, 108630257, 2512352425, 2006195943,
 242057443, 770551691, 4220702018, 3504891840,
 4071859195, 1815727687, 3880753632, 1512370721]


#r13
table_der16_L = [
 1499706267, 821517021, 272624828, 3747450, 903838431, 3785014547, 3673731932, 4168712724,
 3003917061, 108626153, 1761663296, 56526703, 2743431188, 1456005474, 3916730198, 2446648329,
 3704052233, 2892363055, 2238831879, 27188470, 1372518891, 514961789, 180241519, 992443646,
 2218289596, 2990449304, 2083090257, 3447298418, 1645742692, 180376277, 4027015685, 320352659,
 3104361364, 1274334524, 3739604106, 4132412020, 3595179472, 2999637044, 1692991803, 4100768210,
 2641947099, 293114390, 861309414, 122453723, 3874185605, 3491763979, 4077169173, 3170023070,
 1320407786, 3616361820, 2127740868, 1491857651, 2337872959, 3540262763, 6073992, 3127471964,
 3245117150, 2596059943, 1456967031, 1985691168, 3476496685, 422456783, 2360289286, 3164231515,
 2003050958, 1384102379, 1777877824, 2914810839, 1945289513, 1937738367, 3808547812, 92192434,
 1482274904, 751049081, 3966346809, 1404097827, 966980914, 2334778205, 1155714689, 3423641965,
 1155137368, 1997548564, 4216815997, 1022871752, 304252554, 1098110417, 891940402, 2057172366,
 1362437548, 2174045100, 3461598453, 2031519528, 2731958520, 4077701582, 4038630518, 144683945,
 3373915048, 530468291, 2723761676, 889184632, 2957251835, 3542070074, 407700088, 746678938,
 3113480946, 3839385628, 1239609054, 2631208161, 3225296503, 2835449502, 1970921040, 1919599787,
 953174080, 3938325104, 2353037613, 3897128631, 388695213, 1969620676, 1713734801, 3047210744,
 1808555251, 2771758740, 3380250538, 2432936182, 3426459231, 3139485215, 4291817367, 2425704214,
 1343938982, 1859488205, 1747365931, 1449128547, 2391553691, 1771093626, 891664448, 407361467,
 895107271, 2740428560, 3924683950, 3493034673, 1666573754, 2291618645, 1589703631, 2698387773,
 1966642055, 3825342192, 3649096928, 1820903036, 2191344028, 1005995291, 300973050, 3300988238,
 3171366379, 2633555869, 3830955637, 1671871768, 4024926163, 2202147683, 2139293485, 1150553953,
 4173188599, 1640497866, 170981800, 2146450231, 2725037481, 3775813389, 2515143702, 2690542338,
 3357365124, 2005261061, 2462600287, 89671191, 2193424013, 1436459482, 1339411888, 2642878445,
 3715061049, 1322381094, 4268233668, 486175189, 3579919082, 253533068, 734708829, 1708134209,
 1755046104, 1756369152, 1666962048, 2504875339, 1921548153, 3456268917, 4243594342, 6953939,
 429530254, 2279009829, 2475597576, 3634437453, 731532673, 3069563621, 3512480346, 1693102017,
 4248024604, 1136770391, 934867016, 1555725554, 1921060048, 2885612628, 2808317114, 3632665664,
 920541239, 258606065, 282365857, 1485040687, 2831158427, 1636826788, 2690490886, 4290374010,
 2318886785, 827765881, 3836698317, 2279202927, 3033846873, 3952770341, 1507851946, 3700159827,
 1842232378, 560109326, 3905524381, 1815780340, 1171936146, 2632902576, 1063834859, 3442033487,
 239990546, 2980628122, 471404358, 725976892, 1185931209, 1642629183, 2542868679, 1895403953,
 528638029, 3247174799, 3005740287, 1369077000, 4200700685, 646780048, 1451112386, 631273105,
 1111710144, 322047059, 4025959165, 1220101562, 3483952913, 2405414169, 728969893, 279606908]

#r12
table_sched_key = [3593965546, 3795626180, 2229514684, 3462271167,
 2729665754, 1106399472, 266956864, 482366054,
 385384011, 1365124791, 4003761611, 1655920612,
 968695873, 1926051962, 4084903051]

def ROR(x, n, bits = 32):
    mask = (2**n) - 1
    mask_bits = x & mask
    return (x >> n) | (mask_bits << (bits - n))

def ROL(x, n, bits = 32):
    return ROR(x, bits - n, bits)

def derive8(msg):
    L = msg[0]
    R = msg[1]
    for i in range(6):
        t = R + table_enc8[i*2]
        t &= 0xffffffff

        L ^= t

        t = L | table_enc8[i*2 + 1]

        R ^= t
    return [L , R]

def mix8L(t,L):
    t0 = t[0]
    t1 = t[1]
    L0 = L[0]
    L1 = L[1]

    L0 ^= t0
    L1 ^= t1
    L0 = ROL(L0, 4)
    L0 ^= t1
    L1 = ROR(L1, 18)
    L1 ^= t0
    ret = [L0, L1]

    return ret

def mix8R(t,R):
    t0 = t[0]
    t1 = t[1]
    R0 = R[0]
    R1 = R[1]

    R0 ^= t1
    R0 = ROL(R0, 26)
    R1 ^= t0
    R1 ^= t1
    R1 = ROR(R1, 14)
    ret = [R0, R1]
    return ret

def big_func(x):
    if x == 0x84653217:
        return 0x60bf080f
    elif x == 0x17246549:
        return 0x818f694a

#key
def schedule_key(K, round):
    K0,K1,K2,K3 = K

    K0 ^= table_sched_key[round]
    K1 += 0x45786532
    K1 &= 0xffffffff
    K2 ^= K1
    K3 = ROL(K3,4)
    K0 -= K1
    K0 &= 0xffffffff
    if (K2 & 0x80000000):
        a = 0x84653217
    else:
        a = 0x17246549
    a = big_func(a)
    K1 ^= a
    K1 ^= K3
    return [K0,K1,K2,K3]

def ss(l):
    return str(map(hex,l))

def encrypt16_key(msg,key):
    L = msg[:2]
    R = msg[2:]
    K = key

    for i in range(15):
        K = schedule_key(K,i)

        t = derive8(R)
        L = mix8L(t,L)
        L[0] ^= K[0]
        L[1] ^= K[1]
        t = derive8(L)
        R = mix8R(t,R)
        R[0] ^= K[2]
        R[1] ^= K[3]

    return L + R

def derive16(L):
    L0 = L[0]
    L1 = L[1]
    L2 = L[2]
    L3 = L[3]
    for i in range(4):
        L0 ^= L1 + L2
        L0 &= 0xffffffff
        L1 &= L2
        L2 -= L0
        L2 &= 0xffffffff
        L0 += table_der16_L[L3 & 0xff]
        L0 &= 0xffffffff
        L3 >>= 8
        L3 ^= L0
    return [L0,L1,L2,L3]

#in/out : list of dword
def encode32(msg):
    L = msg[:4]
    R = msg[4:]

    for i in range(4):
        print ("encrypt32 : will derivate {}".format(map(hex,R)))
        t = derive16(R)
        print(str(map(hex,t)))
        L = encrypt16_key(L,t)
        print ("encrypted L : {}".format(ss(L)))

        t = derive16(L)
        print("derive L : t {}, R {}".format(ss(t), ss(R)))
        R = encrypt16_key(R,t)
        print ("encrypted R : {}".format(ss(R)))
    crypt = L + R
    return (crypt)



##### decrypt part #######

def inv_mix8R(t,R):
    t0 = t[0]
    t1 = t[1]
    R0 = R[0]
    R1 = R[1]

    R1 = ROL(R1, 14)
    R1 ^= t1
    R1 ^= t0
    R0 = ROR(R0, 26)
    R0 ^= t1
    ret = [R0,R1]
    return ret

def inv_mix8L(t,L):
    t0 = t[0]
    t1 = t[1]
    L0 = L[0]
    L1 = L[1]

    L1 ^= t0
    L1 = ROL(L1,18)
    L0 ^= t1
    L0 = ROR(L0,4)
    L1 ^= t1
    L0 ^= t0
    ret = [L0,L1]

    return ret


def decode32(msg):
    L = msg[:4]
    R = msg[4:]

    for i in range(4):
        t = derive16(L)
        print("derive16L {}".format(t))
        R = decrypt16_key(R,t)

        t = derive16(R)
        print("derive16R {}".format(t))
        L = decrypt16_key(L,t)
    decrypt = L + R
    return (decrypt)

def decrypt16_key(msg,key):
    L = msg[:2]
    R = msg[2:]
    Ks = [schedule_key(key,0)]
    for i in range(1,15):
        Ks.append(schedule_key(Ks[-1],i))

    for i in reversed(range(15)):
        K = Ks[i]
        t = derive8(L)
        R[0] ^= K[2]
        R[1] ^= K[3]
        R = inv_mix8R(t,R)

        t = derive8(R)
        L[0] ^= K[0]
        L[1] ^= K[1]
        L = inv_mix8L(t,L)

    return L + R

######## helpers ###########

def u32(s):
    return struct.unpack("<I",s[:4])[0]

def ba2dw(ba):
    ret = []
    for i in range(8):
        ret.append(u32(ba[i*4:(i+1)*4]))
    return ret

def dw2ba(dw):
    ret = b""
    for d in dw:
        ret += struct.pack("<I",d)
    return ret

def to_hex_string(s):
    ret = ""
    for i in s:
        ret += "\\x{:02x}".format((i))
    return ret

def encode_flag(flag):
    dwflag = ba2dw(flag)
    dweflag = encode32(dwflag)
    return dw2ba(dweflag)


def decode_eflag(eflag):
    dwflag = ba2dw(eflag)
    dweflag = decode32(dwflag)
    return dw2ba(dweflag)


if __name__ == "__main__":

    flag = b"SSTIC{This_is_the_flag_for_beta}"
    eflag = encode_flag(flag)
    eflag = eflag[:-1] + b"A"
    decoded_eflag = decode_eflag(eflag)

    #print ("flag {}".format(flag))
    #print ("encoded flag{}".format(repr(eflag)))
    #print ("encoded hex flag {}".format(to_hex_string(eflag)))
    print ("decoded eflag {}".format(decoded_eflag))


    if 0:
        a = ba2dw(b"\xaa\xbb\xcc\xdd\xee\xff\x00\x11\xaa\xbb\xcc\xdd\xee\xff\x00\x11\xaa\xbb\xcc\xdd\xee\xff\x00\x11\xaa\xbb\xcc\xdd\xee\xff\x00\x11")
        b = dw2ba(a)
        print(repr(b))

        t = b"testflagTESTFLAGtestFLAGTESTflag"
        a = ba2dw(t)
        b = encode32(a)
        print(repr(b))
        c = decode32(b)
        d = dw2ba(c)
        print(d)

        print ("aaa..")

    if 0:
        t = b"aaaabbbbccccddddeeeeffffgggghhhh"
        t = b"dezjh46883dfrpodzd213548qa@deuda"
        a = ba2dw(t)
        print (a)
        #a[0],a[1],a[2],a[3],a[4],a[5],a[6],a[7] = a[4],a[5],a[6],a[7],a[0],a[1],a[2],a[3]
        print (a)
        b = encode32(a)
        print ("encrypted_flag : {}".format(to_hex_string(dw2ba(b))))
        c = decode32(b)
        d = dw2ba(c)
        print(d)

