/*
 * SSTIC Device Driver
 */

#include <linux/arm-smccc.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/platform_device.h>
#include <linux/spinlock.h>
#include <linux/slab.h>
#include <linux/io.h>
#include <linux/of.h>
#include <linux/device.h>
#include <linux/cdev.h>
#include <linux/highmem.h>


static dev_t first;
static struct cdev c_dev;
static struct class *cl;
static int already_open = 0;

#define KEYSTORE_MAX_KEY_SIZE 0x100

typedef enum{
	UNDEFINED,
	RSA_KEY,
	AES_KEY,
	AES_KEY_WRAPPED,
} key_type_t;

struct key_container {
	uint64_t key_id;
	key_type_t key_type;
	uint16_t key_len;
	uint8_t key_data[KEYSTORE_MAX_KEY_SIZE];
};

static int sstic_open(struct inode *inode, struct file *file) {
	//printk(KERN_INFO "sstic: open()\n");
	if (already_open) {
		return -EBUSY;
	}
	already_open++;
	return 0;
}
static int sstic_release(struct inode *inode, struct file *file) {
	//printk(KERN_INFO "sstic: release()\n");
	already_open--;
	return 0;
}


#define SSTIC_IOC_MAGIC   'S'
#define SSTIC_LOAD_MEM    _IOWR(SSTIC_IOC_MAGIC, 0, struct sstic_msg)
#define SSTIC_LOAD_INPUT  _IOWR(SSTIC_IOC_MAGIC, 1, struct sstic_msg)
#define SSTIC_FETCH       _IOWR(SSTIC_IOC_MAGIC, 2, struct sstic_msg)
#define SSTIC_STEP        _IOWR(SSTIC_IOC_MAGIC, 3, struct sstic_msg)
#define SSTIC_TEST        _IOWR(SSTIC_IOC_MAGIC, 4, struct sstic_msg)
#define SSTIC_GET_KEY     _IOWR(SSTIC_IOC_MAGIC, 5, struct key_container)
#define SSTIC_SET_KEY     _IOWR(SSTIC_IOC_MAGIC, 6, struct key_container)
#define MAX_MEM_LOAD_SIZE 0x00101010
#define MAX_MEM_INPUT_SIZE 0x20

#define VM_TEST       0xf2002000
#define VM_FETCH      0xf2005001
#define VM_STEP       0xf2005002
#define VM_LOAD_INPUT 0xf2005003
#define VM_LOAD_CODE  0x83010004
#define KEYSTORE_GET_KEY  0x83010005
#define KEYSTORE_SET_KEY  0x83010006

struct sstic_load_mem {
	uintptr_t zone_ptr;
	uint64_t zone_len;
};

struct sstic_fetch {
	uint32_t result;
};

struct sstic_step {
	uint32_t opcode;
};

struct sstic_msg {
	union {
		struct sstic_load_mem load_mem;
		struct sstic_load_mem load_input;
		struct sstic_fetch fetch;
		struct sstic_step step;
	};
};


static long sstic_ioctl(struct file *file, unsigned int cmd, unsigned long arg) {
    struct arm_smccc_res res = {0};
	struct sstic_msg msg = {0};
	struct key_container * key =0;
	char *buffer;
	int i;
	uint32_t *buffer32;
	void __user *uarg = (void *)arg;


	switch(cmd) {
		case SSTIC_GET_KEY:
			
			key = kmalloc(sizeof(struct key_container), GFP_KERNEL);
			if(!key) {
				return -EFAULT;
			}
			if (copy_from_user(key, uarg, sizeof(struct key_container))) {
				return -EFAULT;
			}
			arm_smccc_smc(
				KEYSTORE_GET_KEY,
				virt_to_phys(key),
				0, 
			0, 0, 0, 0, 0, &res);
			if (copy_to_user(uarg,key,sizeof(struct key_container))) {
				return -EFAULT;
			}
			kfree(key);
			return res.a0;

		case SSTIC_SET_KEY:
			key = kmalloc(sizeof(struct key_container), GFP_KERNEL);
			if(!key) {
				return -EFAULT;
			}
			if (copy_from_user(key, uarg, sizeof(struct key_container))) {
				return -EFAULT;
			}
			arm_smccc_smc(
				KEYSTORE_SET_KEY,
				virt_to_phys(key),
				0, 
			0, 0, 0, 0, 0, &res);
			kfree(key);
			return res.a0;



		case SSTIC_LOAD_MEM:
			if (copy_from_user(&msg, uarg, sizeof(struct sstic_msg))) {
				return -EFAULT;
			}
			if(msg.load_mem.zone_len > MAX_MEM_LOAD_SIZE) {
				return -EFAULT;
			}
			buffer = kmalloc(msg.load_mem.zone_len, GFP_KERNEL);
			if(!buffer) {
				return -EFAULT;
			}
			if (copy_from_user(buffer, (void *)msg.load_mem.zone_ptr, msg.load_mem.zone_len)) {
				return -EFAULT;
			}
    		arm_smccc_smc(
				VM_LOAD_CODE,
				virt_to_phys(buffer),
				msg.load_mem.zone_len, 
			0, 0, 0, 0, 0, &res);
			kfree(buffer);
			return res.a0;
		case SSTIC_LOAD_INPUT:
			if (copy_from_user(&msg, uarg, sizeof(struct sstic_msg))) {
				return -EFAULT;
			}
			if(msg.load_input.zone_len != MAX_MEM_INPUT_SIZE) {
				return -EFAULT;
			}
			buffer32 = (uint32_t *)kmalloc(msg.load_input.zone_len, GFP_KERNEL);
			if(!buffer32) {
				return -EFAULT;
			}
			if (copy_from_user(buffer32, (void *)msg.load_input.zone_ptr, msg.load_input.zone_len)) {
				return -EFAULT;
			}
			for(i=0;i<8; i++) {
				arm_smccc_smc(VM_LOAD_INPUT, i, buffer32[i], 0, 0, 0, 0, 0, &res);
			}
			kfree(buffer32);
			return res.a0;
		case SSTIC_FETCH:
			arm_smccc_smc(VM_FETCH, 0, 0, 0, 0, 0, 0, 0, &res);
			return 0;
		case SSTIC_STEP:
			arm_smccc_smc(VM_STEP, 0, 0, 0, 0, 0, 0, 0, &res);
			return (((res.a1)&0xffff)|((res.a2&0xffff)<<16));
		case SSTIC_TEST:
			arm_smccc_smc(VM_TEST, 0, 0, 0, 0, 0, 0, 0, &res);
			return 0;
		default:
			return -ENOIOCTLCMD;
	}
	return -ENOIOCTLCMD;
}


static struct file_operations pugs_fops = {
	.owner = THIS_MODULE,
	.open = sstic_open,
	.release = sstic_release,
	.unlocked_ioctl = sstic_ioctl,
};

static int __init sstic_init(void) {
	/* create chardev */
	if (alloc_chrdev_region(&first, 0, 1, "sstic") < 0) {
		return -1;
	}
	if ((cl = class_create(THIS_MODULE, "chardrv")) == NULL) {
		unregister_chrdev_region(first, 1);
		return -1;
	}
	if (device_create(cl, NULL, first, NULL, "sstic") == NULL) {
		class_destroy(cl);
		unregister_chrdev_region(first, 1);
		return -1;
	}
	cdev_init(&c_dev, &pugs_fops);
	if (cdev_add(&c_dev, first, 1) == -1)
	{
		device_destroy(cl, first);
		class_destroy(cl);
		unregister_chrdev_region(first, 1);
		return -1;
	}
	return 0;
}

static void __exit sstic_exit(void) {
	unregister_chrdev_region(first, 3);
}

module_init(sstic_init);
module_exit(sstic_exit);

MODULE_AUTHOR("David BERARD <contact@davidberard.fr>");
MODULE_DESCRIPTION("Driver for SSTIC Challenge : Communication between normal world and Secure world");
MODULE_LICENSE("GPL");
MODULE_ALIAS("platform:sstic");
