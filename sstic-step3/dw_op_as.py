#This is a small assembler for
#dwarf2 OP opcode (and only thoses) with
#basic support of label, goto, jnz
#some meta instr like PACK/UNPACK usefull for the algo

import sys
import struct
import algo_check


ADDRESS_OK = 0x0403098
ADDRESS_NOK = 0x04030B8

address_code = 0x400258

flag = b"SSTIC{Dw4rf_VM_1s_co0l_isn_t_It}"
FLAG = algo_check.encode_flag(flag)


CODE_LENGTH=1000 #now it's actually the code before tables


table1 = [1218305502, 108630257, 2512352425, 2006195943,
           242057443, 770551691, 4220702018, 3504891840,
           4071859195, 1815727687, 3880753632, 1512370721]

table2 = [3593965546, 3795626180, 2229514684, 3462271167,
  2729665754, 1106399472, 266956864, 482366054,
  385384011, 1365124791, 4003761611, 1655920612,
  968695873, 1926051962, 4084903051]

table3 =  [
     1499706267, 821517021, 272624828, 3747450, 903838431, 3785014547, 3673731932, 4168712724,
     3003917061, 108626153, 1761663296, 56526703, 2743431188, 1456005474, 3916730198, 2446648329,
     3704052233, 2892363055, 2238831879, 27188470, 1372518891, 514961789, 180241519, 992443646,
     2218289596, 2990449304, 2083090257, 3447298418, 1645742692, 180376277, 4027015685, 320352659,
     3104361364, 1274334524, 3739604106, 4132412020, 3595179472, 2999637044, 1692991803, 4100768210,
     2641947099, 293114390, 861309414, 122453723, 3874185605, 3491763979, 4077169173, 3170023070,
     1320407786, 3616361820, 2127740868, 1491857651, 2337872959, 3540262763, 6073992, 3127471964,
     3245117150, 2596059943, 1456967031, 1985691168, 3476496685, 422456783, 2360289286, 3164231515,
     2003050958, 1384102379, 1777877824, 2914810839, 1945289513, 1937738367, 3808547812, 92192434,
     1482274904, 751049081, 3966346809, 1404097827, 966980914, 2334778205, 1155714689, 3423641965,
     1155137368, 1997548564, 4216815997, 1022871752, 304252554, 1098110417, 891940402, 2057172366,
     1362437548, 2174045100, 3461598453, 2031519528, 2731958520, 4077701582, 4038630518, 144683945,
     3373915048, 530468291, 2723761676, 889184632, 2957251835, 3542070074, 407700088, 746678938,
     3113480946, 3839385628, 1239609054, 2631208161, 3225296503, 2835449502, 1970921040, 1919599787,
     953174080, 3938325104, 2353037613, 3897128631, 388695213, 1969620676, 1713734801, 3047210744,
     1808555251, 2771758740, 3380250538, 2432936182, 3426459231, 3139485215, 4291817367, 2425704214,
     1343938982, 1859488205, 1747365931, 1449128547, 2391553691, 1771093626, 891664448, 407361467,
     895107271, 2740428560, 3924683950, 3493034673, 1666573754, 2291618645, 1589703631, 2698387773,
     1966642055, 3825342192, 3649096928, 1820903036, 2191344028, 1005995291, 300973050, 3300988238,
     3171366379, 2633555869, 3830955637, 1671871768, 4024926163, 2202147683, 2139293485, 1150553953,
     4173188599, 1640497866, 170981800, 2146450231, 2725037481, 3775813389, 2515143702, 2690542338,
     3357365124, 2005261061, 2462600287, 89671191, 2193424013, 1436459482, 1339411888, 2642878445,
     3715061049, 1322381094, 4268233668, 486175189, 3579919082, 253533068, 734708829, 1708134209,
     1755046104, 1756369152, 1666962048, 2504875339, 1921548153, 3456268917, 4243594342, 6953939,
     429530254, 2279009829, 2475597576, 3634437453, 731532673, 3069563621, 3512480346, 1693102017,
     4248024604, 1136770391, 934867016, 1555725554, 1921060048, 2885612628, 2808317114, 3632665664,
     920541239, 258606065, 282365857, 1485040687, 2831158427, 1636826788, 2690490886, 4290374010,
     2318886785, 827765881, 3836698317, 2279202927, 3033846873, 3952770341, 1507851946, 3700159827,
     1842232378, 560109326, 3905524381, 1815780340, 1171936146, 2632902576, 1063834859, 3442033487,
     239990546, 2980628122, 471404358, 725976892, 1185931209, 1642629183, 2542868679, 1895403953,
     528638029, 3247174799, 3005740287, 1369077000, 4200700685, 646780048, 1451112386, 631273105,
     1111710144, 322047059, 4025959165, 1220101562, 3483952913, 2405414169, 728969893, 279606908]

import struct
def table_to_bin(table):
    ret = b""
    for i in table:
        ret += struct.pack("<I",i)
    return ret

table1_bin = table_to_bin(table1)
table2_bin = table_to_bin(table2)
table3_bin = table_to_bin(table3)
tables_bin = table1_bin + table2_bin + table3_bin







opcodes = { "DW_OP_addr": 0x03,
"DW_OP_deref": 0x06,
"DW_OP_const1u": 0x08,
"DW_OP_const1s": 0x09,
"DW_OP_const2u": 0x0a,
"DW_OP_const2s": 0x0b,
"DW_OP_const4u": 0x0c,
"DW_OP_const4s": 0x0d,
"DW_OP_const8u": 0x0e,
"DW_OP_const8s": 0x0f,
"DW_OP_constu": 0x10,
"DW_OP_consts": 0x11,
"DW_OP_dup": 0x12,
"DW_OP_drop": 0x13,
"DW_OP_over": 0x14,
"DW_OP_pick": 0x15,
"DW_OP_swap": 0x16,
"DW_OP_rot": 0x17,
"DW_OP_xderef": 0x18,
"DW_OP_abs": 0x19,
"DW_OP_and": 0x1a,
"DW_OP_div": 0x1b,
"DW_OP_minus": 0x1c,
"DW_OP_mod": 0x1d,
"DW_OP_mul": 0x1e,
"DW_OP_neg": 0x1f,
"DW_OP_not": 0x20,
"DW_OP_or": 0x21,
"DW_OP_plus": 0x22,
"DW_OP_plus_uconst": 0x23,
"DW_OP_shl": 0x24,
"DW_OP_shr": 0x25,
"DW_OP_shra": 0x26,
"DW_OP_xor": 0x27,
"DW_OP_bra": 0x28,
"DW_OP_eq": 0x29,
"DW_OP_ge": 0x2a,
"DW_OP_gt": 0x2b,
"DW_OP_le": 0x2c,
"DW_OP_lt": 0x2d,
"DW_OP_ne": 0x2e,
"DW_OP_skip": 0x2f,
"DW_OP_lit0": 0x30,
"DW_OP_lit1": 0x31,
"DW_OP_lit2": 0x32,
"DW_OP_lit3": 0x33,
"DW_OP_lit4": 0x34,
"DW_OP_lit5": 0x35,
"DW_OP_lit6": 0x36,
"DW_OP_lit7": 0x37,
"DW_OP_lit8": 0x38,
"DW_OP_lit9": 0x39,
"DW_OP_lit10": 0x3a,
"DW_OP_lit11": 0x3b,
"DW_OP_lit12": 0x3c,
"DW_OP_lit13": 0x3d,
"DW_OP_lit14": 0x3e,
"DW_OP_lit15": 0x3f,
"DW_OP_lit16": 0x40,
"DW_OP_lit17": 0x41,
"DW_OP_lit18": 0x42,
"DW_OP_lit19": 0x43,
"DW_OP_lit20": 0x44,
"DW_OP_lit21": 0x45,
"DW_OP_lit22": 0x46,
"DW_OP_lit23": 0x47,
"DW_OP_lit24": 0x48,
"DW_OP_lit25": 0x49,
"DW_OP_lit26": 0x4a,
"DW_OP_lit27": 0x4b,
"DW_OP_lit28": 0x4c,
"DW_OP_lit29": 0x4d,
"DW_OP_lit30": 0x4e,
"DW_OP_lit31": 0x4f,
"DW_OP_reg0": 0x50,
"DW_OP_reg1": 0x51,
"DW_OP_reg2": 0x52,
"DW_OP_reg3": 0x53,
"DW_OP_reg4": 0x54,
"DW_OP_reg5": 0x55,
"DW_OP_reg6": 0x56,
"DW_OP_reg7": 0x57,
"DW_OP_reg8": 0x58,
"DW_OP_reg9": 0x59,
"DW_OP_reg10": 0x5a,
"DW_OP_reg11": 0x5b,
"DW_OP_reg12": 0x5c,
"DW_OP_reg13": 0x5d,
"DW_OP_reg14": 0x5e,
"DW_OP_reg15": 0x5f,
"DW_OP_reg16": 0x60,
"DW_OP_reg17": 0x61,
"DW_OP_reg18": 0x62,
"DW_OP_reg19": 0x63,
"DW_OP_reg20": 0x64,
"DW_OP_reg21": 0x65,
"DW_OP_reg22": 0x66,
"DW_OP_reg23": 0x67,
"DW_OP_reg24": 0x68,
"DW_OP_reg25": 0x69,
"DW_OP_reg26": 0x6a,
"DW_OP_reg27": 0x6b,
"DW_OP_reg28": 0x6c,
"DW_OP_reg29": 0x6d,
"DW_OP_reg30": 0x6e,
"DW_OP_reg31": 0x6f,
"DW_OP_breg0": 0x70,
"DW_OP_breg1": 0x71,
"DW_OP_breg2": 0x72,
"DW_OP_breg3": 0x73,
"DW_OP_breg4": 0x74,
"DW_OP_breg5": 0x75,
"DW_OP_breg6": 0x76,
"DW_OP_breg7": 0x77,
"DW_OP_breg8": 0x78,
"DW_OP_breg9": 0x79,
"DW_OP_breg10": 0x7a,
"DW_OP_breg11": 0x7b,
"DW_OP_breg12": 0x7c,
"DW_OP_breg13": 0x7d,
"DW_OP_breg14": 0x7e,
"DW_OP_breg15": 0x7f,
"DW_OP_breg16": 0x80,
"DW_OP_breg17": 0x81,
"DW_OP_breg18": 0x82,
"DW_OP_breg19": 0x83,
"DW_OP_breg20": 0x84,
"DW_OP_breg21": 0x85,
"DW_OP_breg22": 0x86,
"DW_OP_breg23": 0x87,
"DW_OP_breg24": 0x88,
"DW_OP_breg25": 0x89,
"DW_OP_breg26": 0x8a,
"DW_OP_breg27": 0x8b,
"DW_OP_breg28": 0x8c,
"DW_OP_breg29": 0x8d,
"DW_OP_breg30": 0x8e,
"DW_OP_breg31": 0x8f,
"DW_OP_regx": 0x90,
"DW_OP_fbreg": 0x91,
"DW_OP_bregx": 0x92,
"DW_OP_piece": 0x93,
"DW_OP_deref_size": 0x94,
"DW_OP_xderef_size": 0x95,
"DW_OP_nop": 0x96 }

def print_c_hex(buf):
    s = "\""
    for i in range(len(buf)):
        if not i % 32 and i:
            s += "\" \\ \n +b\""
        s += "\\x{:02x}".format(buf[i])
    s += "\""
    print(s)

def pu64(x):
    return struct.pack("<Q",x)

def uu64(x):
    return struct.unpack("<Q",x)[0]


def ps64(x):
    return struct.pack("<q",x)

def pu32(x):
    return struct.pack("<I",x)

def ps32(x):
    return struct.pack("<i",x)

def pu16(x):
    return struct.pack("<H",x)

def ps16(x):
    return struct.pack("<h",x)

def pu8(x):
    return struct.pack("<B",x)

def ps8(x):
    return struct.pack("<b",x)

subst_opcode = {"PACK":["shl 32", "or"],
               "UNPACK" : ["dup",
                           "const8u 0xffffffff",
                           "and",
                           "swap",
                           "const8u 0xffffffff00000000",
                           "and",
                           "shr 32"],
               }

subst_operand = {"ADDRESS_OK" : "0x{:x}".format(ADDRESS_OK),
                "ADDRESS_NOK" : "0x{:x}".format(ADDRESS_NOK),
                 "FLAG0" : "0x{:x}".format(uu64(FLAG[0:8])),
                "FLAG1" : "0x{:x}".format(uu64(FLAG[8:16])),
                "FLAG2" : "0x{:x}".format(uu64(FLAG[16:24])),
                "FLAG3" : "0x{:x}".format(uu64(FLAG[24:32]))
}

#functions called more than once

def preprocess(dwlines):
    ret = []
    for codeline in dwlines:
        codeline = codeline.lstrip()
        codeline = codeline.rstrip()
        #print (codeline)
        if codeline == "":
            continue

        if codeline[0] == '#':
            ret.append(codeline)
            continue
        opcode_s, *op1 = codeline.split(" ")
        print (repr((opcode_s)))
        if op1:
            op1 = op1.pop()
        if op1 and op1 in subst_operand.keys():
            print (repr(op1))
            ret.append("{} {}".format(opcode_s, subst_operand[op1]))
        elif opcode_s in subst_opcode.keys():
            ret += subst_opcode[opcode_s]
        elif opcode_s == "call":
            ret.append("goto {}".format(op1))
            ret.append("label ret_{}".format(op1))
        elif opcode_s == "ret":
            ret.append("goto ret_{}".format(op1))
        elif opcode_s == "ROL":
            ret.append("dup")
            ret.append("shl {}".format(op1))
            ret.append("swap")
            ret.append("shr {}".format(32-int(op1)))
            ret.append("or")
            ret.append("const4u 0xffffffff")
            ret.append("and")
        elif opcode_s == "ROR":
            ret.append("dup")
            ret.append("shr {}".format(op1))
            ret.append("swap")
            #print (op1)
            ret.append("shl {}".format(32-int(op1)))
            ret.append("or")
            ret.append("const4u 0xffffffff")
            ret.append("and")
        else:
            ret.append(codeline)
    print (ret)
    return ret



labels = {}
debug_output = ""
def assemble(dwlines, pass1=False):
    global label
    assembled_code = b""
    cur_func = ""

    for codeline in dwlines:
        #codeline = codeline.lstrip()
        codeline = codeline.rstrip()
        print (hex(len(assembled_code) + address_code) + " : " + codeline)
        if codeline == "":
            continue

        #print ((codeline[0]))
        if codeline[0] == '#':
            continue
        cur_ass = b""
        opcode_s, *op1 = codeline.split(" ")
        #print (repr(opcode_s))
        if op1:
            op1 = op1.pop()

        if opcode_s not in ["label","goto","call",'jnz',"func","shr","shl","TABLES"]:
            opcode = opcodes["DW_OP_"+opcode_s]
            cur_ass += bytes([opcode])
            if op1:
                op1 = int(op1,16)

        if opcode_s == "addr":
            cur_ass += pu64(op1)
        elif opcode_s == "const1s":
            cur_ass += ps8(op1)
        elif opcode_s == "const1u":
            cur_ass += pu8(op1)
        elif opcode_s == "const2s":
            cur_ass += ps16(op1)
        elif opcode_s == "const2u":
            cur_ass += pu16(op1)
        elif opcode_s == "const4s":
            cur_ass += ps32(op1)
        elif opcode_s == "const4u":
            cur_ass += pu32(op1)
        elif opcode_s == "const8s":
            cur_ass += ps64(op1)
        elif opcode_s == "const8u":
            cur_ass += pu64(op1)
        elif opcode_s == "pick":
            cur_ass += pu8(op1)
        elif opcode_s == "deref_size":
            cur_ass += pu8(op1)
        elif opcode_s == "shr":
            op1 = int(op1)
            cur_ass += bytes([opcodes["DW_OP_const1u"]])
            cur_ass += pu8(op1)
            cur_ass += bytes([opcodes["DW_OP_shr"]])
        elif opcode_s == "shl":
            op1 = int(op1)
            cur_ass += bytes([opcodes["DW_OP_const1u"]])
            cur_ass += pu8(op1)
            cur_ass += bytes([opcodes["DW_OP_shl"]])
        elif opcode_s == "skip":
            cur_ass += ps16(op1)
        elif opcode_s == "bra":
            cur_ass += ps16(op1)
        elif opcode_s == "label" or opcode_s == "func":
            if opcode_s == "func":
                cur_func = op1
            if pass1:
                if "loc_" in op1:
                    op1 = op1.replace("loc_",cur_func)
                labels[op1] = len(assembled_code)
        elif opcode_s == "goto":
            if "loc_" in op1:
                op1 = op1.replace("loc_",cur_func)
            if pass1:
                cur_ass += b"\x00"*3
            else:
                cur_len = len(assembled_code) + 3
                dest_len = labels[op1]
                cur_ass += bytes([opcodes["DW_OP_skip"]])
                cur_ass += ps16(dest_len - cur_len)
        elif opcode_s == "jnz":
            if "loc_" in op1:
                op1 = op1.replace("loc_",cur_func)

            if pass1:
                cur_ass += b"\x00"*3
            else:
                cur_len = len(assembled_code) + 3
                dest_len = labels[op1]
                #print ("dest len : {:x}, cur_len {:x}".format(dest_len, cur_len))
                cur_ass += bytes([opcodes["DW_OP_bra"]])
                #print(dest_len - cur_len)
                #print (repr(cur_ass))
                cur_ass += ps16(dest_len - cur_len)
                #print (repr(cur_ass))
        elif opcode_s == "TABLES":
            cur_ass += b"\x96" * (CODE_LENGTH - len(assembled_code))
            cur_ass += b"\x00" * 8
            cur_ass += tables_bin
            cur_ass += b"\xc0"




        assembled_code += cur_ass
    return assembled_code

import binascii
if __name__ == "__main__":
    dwlines = open(sys.argv[1],"r").readlines()
    dwlines = preprocess(dwlines)
    ac = assemble(dwlines,pass1=True)
    print (labels)
    ac = assemble(dwlines)
    ac += b"\x96"*(1000 - len(ac))

    print_c_hex(ac)
    print ("encoded flag : {}".format(binascii.hexlify(FLAG)))






